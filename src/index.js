import React from 'react';
import ReactDOM from 'react-dom';
import GuestList from './components/templates/GuestList';

ReactDOM.render(<GuestList />, document.getElementById('root'));