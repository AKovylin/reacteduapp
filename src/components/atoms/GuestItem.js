import React, { Component } from 'react';
import '../../styles/main.css';

class GuestItem extends Component {
  state = {
    isChecked: false
  }

  onCheck = ({target}) => this.setState({ isChecked: target.checked });

  render = () => {
    const { onCheck } = this;
    const { guestName } = this.props;
    const { isChecked } = this.state;

    return (
      <label className = {isChecked && 'label-checked'}>
        {guestName}
        <input
          name={"guest-" + guestName}
          type="checkbox"
          checked={isChecked}
          onChange={onCheck}
          />
        </label>
    )
  } 
}

export default GuestItem;