import React, { Component } from 'react';
import '../../styles/main.css';
import GuestItem from '../atoms/GuestItem'
const guestList = ['Ben', 'Lora', 'Will', 'Chris', 'Joe'];

class GuestList extends Component {
  state = {
    guests: guestList || [],
  }

  getfilteredSearchResults = ({target}) => {
    this.setState({
      guests: guestList.filter(guest => guest.toLowerCase().includes(target.value.toLowerCase()))
    });
  };

  render = () => {
    const { getfilteredSearchResults } = this;
    const { guests } = this.state;
    const guestLabel = guests.map((guest, i) => <li key={i}><GuestItem guestName={guest}/></li>);
    return (
      <div>
        <div><label>Search: </label><input type='text' onChange={getfilteredSearchResults} /></div>
        <div><ul>{guestLabel}</ul></div>
      </div>
    )
  } 
}

export default GuestList;